#!/user/bin/env pyhton
''' sample for devops '''

class Account:
    ''' Bank Account '''

    def __init__(self, name, balance):
        ''' init '''
        self.name = name
        self.balance = balance

    def deposit(self, value):
        ''' deposit '''
        self.balance += value

    def withdraw(self, value):
        ''' withdraw '''
        self.balance -=value

if __name__ == '__main__':
    acc = Account('a', 90)
    acc.deposit(10)
